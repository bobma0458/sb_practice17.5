﻿#include <iostream>


class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector(double x, double y, double z) : x(x), y(y), z(z) {}

    void Values() const
    {
        std::cout << "Значение вектора: x = " << x << ", y = " << y << ", z = " << z << "\n";
    }

    double getLength() const
    {
        return hypot(hypot(x, y), z);
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");

    Vector newVector(2, 4, 6);

    newVector.Values();
    std::cout << "Длина вектора: " << newVector.getLength() << "\n";

    return 0;
}
